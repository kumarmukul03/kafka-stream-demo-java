package com.wba.api.processrxinfo;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"classpath:features"},
        plugin = {"pretty", "html:target/cucumber", "json:target/cucumber/cucumber.json"},
        glue = {"com.wba.testing.bdd.api.steps"}
)
public class RunCucumberTest {

}