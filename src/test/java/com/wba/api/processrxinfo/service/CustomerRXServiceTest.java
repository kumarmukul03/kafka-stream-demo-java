package com.wba.api.processrxinfo.service;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.wba.api.processrxinfo.model.CustomerRXInfo;
import com.wba.api.processrxinfo.model.RXInfo;
import com.wba.api.processrxinfo.repository.CustomerRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerRXServiceTest {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    CustomerRXServiceImpl customerRXService;


    CustomerRXInfo customerRXInfo;
    RXInfo rxInfo;
    String customerID;
    Logger RzkstreamProcesorLog;
    ListAppender<ILoggingEvent> listAppender;

    @Before
    public void setup() {

        customerID = "TEST001";
        customerRXInfo = new CustomerRXInfo();
        customerRXInfo.setId(customerID);
        customerRXInfo.setPatientId(customerID);

        rxInfo = new RXInfo();
        rxInfo.setDrugId("TDRUG001");

        customerRXInfo.getRxList().add(rxInfo);

        RzkstreamProcesorLog = (Logger) LoggerFactory.getLogger(CustomerRXServiceImpl.class);
        listAppender = new ListAppender<>();
        listAppender.start();
        RzkstreamProcesorLog.addAppender(listAppender);
    }

    @Test
    public void insertOrUpdateCustomerRxObj_isabletoSave() {

        customerRXService.insertOrUpdateCustomerRxObj(customerRXInfo, customerID);
        assertEquals(customerRXInfo, customerRepository.findById(customerID).get());
    }

    @Test
    public void insertOrUpdateCustomerRxObj_isabletoUpdate() {


        customerRepository.save(customerRXInfo);//prerequisite is data already available in DB

        CustomerRXInfo customerRXInfoNew = new CustomerRXInfo(); // build new customerRX info
        customerRXInfoNew.setId(customerID);
        customerRXInfoNew.setPatientId(customerID);
        customerRXInfoNew.setCity("CITY_NEW");
        customerRXInfoNew.setCardType("CARD_NEW");
        rxInfo = new RXInfo();
        rxInfo.setDrugId("TDRUG001_NEW");
        customerRXInfoNew.getRxList().add(rxInfo);

        customerRXService.insertOrUpdateCustomerRxObj(customerRXInfoNew, customerID);
        customerRXInfoNew.getRxList().clear();
        customerRXInfoNew.getRxList().add(rxInfo);
        customerRXInfoNew.getRxList().addAll(customerRXInfo.getRxList());

        assertEquals(customerRXInfoNew, customerRepository.findById(customerID).get());
    }

    @Test
    public void insertOrUpdateCustomerRxObj_handlingDuplicatedRXinfoinput() {

        customerRepository.save(customerRXInfo);
        customerRXService.insertOrUpdateCustomerRxObj(customerRXInfo, customerID);

        assertEquals(1, customerRepository.findById(customerID).get().getRxList().size());
        assertEquals(rxInfo, customerRepository.findById(customerID).get().getRxList().get(0));

    }
}
