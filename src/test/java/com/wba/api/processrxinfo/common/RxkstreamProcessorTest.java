package com.wba.api.processrxinfo.common;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wba.api.processrxinfo.dto.CustomerDto;
import com.wba.api.processrxinfo.dto.RxDto;
import com.wba.api.processrxinfo.model.CustomerRXInfo;
import com.wba.api.processrxinfo.model.RXInfo;
import com.wba.api.processrxinfo.repository.CustomerRepository;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class RxkstreamProcessorTest {

    final String CUSTOMER_INFOTOPIC = "customerInfo";
    final String RX_INFOTOPIC = "rxInfo";
    TopologyTestDriver testDriver;
    String customerInfoTestData;
    String rxInfoTestData1;
    String rxInfoTestData2;
    String topicKey;
    Logger rzkstreamProcesorLog;
    ListAppender<ILoggingEvent> listAppender;

    @Autowired
    RxkstreamProcessor rxkstreamProcessor;

    @Autowired
    CustomerRepository customerRepository;

    @Before
    public void setup() {

        topicKey = "58519001004";

        customerInfoTestData = "{\n" +
                "  \"patientId\": \"58519001004\",\n" +
                "  \"firstName\": \"nŮźDåŷ:ė\",\n" +
                "  \"middleInit\": \"MA\",\n" +
                "  \"lastName\": \"nŮźDåŷ:ė\",\n" +
                "  \"surnameSuffix\": \"Sr\",\n" +
                "  \"gender\": \"male\",\n" +
                "  \"email\": \"nŮźDåŷ:ė\",\n" +
                "  \"dob\": \"08/25/1909\",\n" +
                "  \"phoneNumberAreaCode\": \"nŮźDåŷ:ė\",\n" +
                "  \"phoneNumber\": \"nŮźDåŷ:ė\",\n" +
                "  \"preferredStoreNumber\": 789,\n" +
                "  \"lastFilledStoreNumber\": \"248\",\n" +
                "  \"preferredPaymentMethod\": \"creditCard\",\n" +
                "  \"previousFilledLastMile\": \"8455\",\n" +
                "  \"addressLine1\": \"nŮźDåŷ:ė\",\n" +
                "  \"city\": \"nŮźDåŷ:ė\",\n" +
                "  \"zipCode\": \"Z!!@%&@0)\",\n" +
                "  \"state\": \"California\",\n" +
                "  \"cardType\": \"Creaditcard\",\n" +
                "  \"creditCard\": \"XAS11599XXX003\",\n" +
                "  \"lastFourDigits\": 7795,\n" +
                "  \"expiryMonth\": 12,\n" +
                "  \"expiryYear\": 2025,\n" +
                "  \"zipCode\": \"ZASSAQ\"\n" +
                "}";

        rxInfoTestData1 = "{\n" +
                "\"patientId\":\"P001\",\n" +
                "\"rxNumber\":\"0XA145\",\n" +
                "\"rxDate\":\"08/25/1909\",\n" +
                "\"prescriber\":\"AZSDDD\",\n" +
                "\"drugId\":\"ZA12342\",\n" +
                "\"storeId\":\"QQ14578\"\n" +
                "}";
        rxInfoTestData2 = "{\n" +
                "\"patientId\":\"P002\",\n" +
                "\t\"rxNumber\":\"0XA146\",\t\n" +
                "\t\"rxDate\":\"08/25/1909\",\n" +
                "\t\"prescriber\":\"AZSDDD\",\n" +
                "\t\"drugId\":\"ZA12342\",\n" +
                "\t\"storeId\":\"QQ14578\"\n" +
                "}";

        // get Logback Logger
        rzkstreamProcesorLog = (Logger) LoggerFactory.getLogger(RxkstreamProcessor.class);

        // create and start a ListAppender
        listAppender = new ListAppender<>();
        listAppender.start();

        // add the appender to the logger
        // addAppender is outdated now
        rzkstreamProcesorLog.addAppender(listAppender);

        customerRepository.deleteAll();//clear DB
        testDriver = new TopologyTestDriver(rxkstreamProcessor.createTopology(CUSTOMER_INFOTOPIC, RX_INFOTOPIC), rxkstreamProcessor.getConfig("Test001", "localhost:9092"));
    }

    @Test
    public void isAbleToCombineAndSaveStreamData() throws Exception{

        insertKstreamData();
        CustomerRXInfo customerRXInfo = customerRepository.findById(topicKey).get();

        //build expected Object
        CustomerRXInfo customerRXInfoExpected=(CustomerRXInfo) convertStringToObj(customerInfoTestData,true,false);
        RXInfo rxInfo=(RXInfo) convertStringToObj(rxInfoTestData1,false,true);
        customerRXInfoExpected.getRxList().add(rxInfo);

        assertEquals(customerRXInfoExpected, customerRXInfo);
    }

    @Test
    public void isAbleToUpdateRecordWithMultipleRXInfo() {

        insertMultipleRXKstreamData();

        CustomerRXInfo customerRXInfo = customerRepository.findById(topicKey).get();
        assertEquals(2, customerRXInfo.getRxList().size());

    }

    @Test
    public void genrateRecordFromMultipleStreamData(){

        insertMultipleRxCustomerstreamData("TESTK01","TESTK02");
        assertEquals(2, customerRepository.findById("TESTK01").get().getRxList().size());
        assertEquals(2, customerRepository.findById("TESTK02").get().getRxList().size());
    }

    @Test
    public void logJunkDataInfo() {

        insertJunkData();
        logAssertion();
    }

    /**
     * Function for inserting single stream data with same key in both customer&Rx topic
     */
    public void insertKstreamData() {

        ConsumerRecordFactory<String, String> factory = new ConsumerRecordFactory<>(CUSTOMER_INFOTOPIC, new StringSerializer(), new StringSerializer());
        testDriver.pipeInput(factory.create(CUSTOMER_INFOTOPIC, topicKey, customerInfoTestData));

        factory = new ConsumerRecordFactory<>(RX_INFOTOPIC, new StringSerializer(), new StringSerializer());
        testDriver.pipeInput(factory.create(RX_INFOTOPIC, topicKey, rxInfoTestData1));
    }

    /**
     * Function for inserting multiple stream data with same key in both customer&Rx topic
     */
    public void insertMultipleRXKstreamData() {

        List<KeyValue<String, String>> customerKeyValueList = new ArrayList<>();
        customerKeyValueList.add(new KeyValue(topicKey, customerInfoTestData));

        List<KeyValue<String, String>> rxinfoKeyValueList = new ArrayList<>();
        rxinfoKeyValueList.add(new KeyValue(topicKey, rxInfoTestData1));
        rxinfoKeyValueList.add(new KeyValue(topicKey, rxInfoTestData2));

        ConsumerRecordFactory<String, String> factory = new ConsumerRecordFactory<>(CUSTOMER_INFOTOPIC, new StringSerializer(), new StringSerializer());

        testDriver.pipeInput(factory.create(CUSTOMER_INFOTOPIC, customerKeyValueList));
        testDriver.pipeInput(factory.create(RX_INFOTOPIC, rxinfoKeyValueList));
    }

    /**
     * Function for inserting junk data in RX info topic
     */
    public void insertJunkData() {

        ConsumerRecordFactory<String, String> factory = new ConsumerRecordFactory<>(CUSTOMER_INFOTOPIC, new StringSerializer(), new StringSerializer());
        testDriver.pipeInput(factory.create(CUSTOMER_INFOTOPIC, topicKey, "{'ttytyevv':'df'}"));

        factory = new ConsumerRecordFactory<>(RX_INFOTOPIC, new StringSerializer(), new StringSerializer());
        testDriver.pipeInput(factory.create(RX_INFOTOPIC, topicKey, "{'ttytyevv':'df'}"));

    }

    /**
     * Function for simultaneously inserting multiple stream data with same key in both customer&Rx topic
     */
    public void insertMultipleRxCustomerstreamData(String key1,String key2) {

        Map<String, String> customerInfomap = Stream.of(
                new AbstractMap.SimpleEntry<>(key1, "{\"patientId\": \"TD001\",\"firstName\": \"NAM001\"}"),
                new AbstractMap.SimpleEntry<>(key2, "{\"patientId\": \"TD002\",\"firstName\": \"NAM001\"}")
        ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        ArrayList<String> rxInfoList = new ArrayList<String>() {
            {
                add("{\"rxNumber\":\"0XA145\",\"rxDate\":\"08/25/1909\"}");
                add("{\"rxNumber\":\"0XA146\",\"rxDate\":\"08/25/1909\"}");
            }
        };

        Map<String, List<String> > rxInfomap =new HashMap<>();
        rxInfomap.put(key1,rxInfoList);
        rxInfomap.put(key2,rxInfoList);

        List<KeyValue<String, String>> customerKeyValueList = new ArrayList<>();
        customerInfomap.entrySet().forEach(entry->customerKeyValueList.add(new KeyValue(entry.getKey(), entry.getValue())));

        List<KeyValue<String, String>> rxinfoKeyValueList = new ArrayList<>();
        rxInfomap.entrySet().forEach(entry-> {

            entry.getValue().forEach(value->rxinfoKeyValueList.add(new KeyValue(entry.getKey(), value)));
        });

        ConsumerRecordFactory<String, String> factory = new ConsumerRecordFactory<>(CUSTOMER_INFOTOPIC, new StringSerializer(), new StringSerializer());

        testDriver.pipeInput(factory.create(CUSTOMER_INFOTOPIC, customerKeyValueList));
        testDriver.pipeInput(factory.create(RX_INFOTOPIC, rxinfoKeyValueList));
    }

    /**
     * Assertion check on log info
     */
    public void logAssertion() {

        List<ILoggingEvent> logsList = listAppender.list;

        // JUnit assertions
        assertEquals("Exception occurs while building CustomerRXInfo from kafka stream input...", logsList.get(0).getMessage());
        assertEquals(Level.ERROR, logsList.get(0).getLevel());
    }

    /**
     * Creating object from json string ...
     */
    public Object convertStringToObj(String streamData,boolean isCustomerString,boolean isRxString) throws Exception{

        ObjectMapper objectMapper = new ObjectMapper();

        if(isCustomerString){

            CustomerDto customerDto = objectMapper.readValue(streamData, CustomerDto.class);
            ModelMapper modelMapper = new ModelMapper();
            CustomerRXInfo customerRXInfo = new CustomerRXInfo();
            modelMapper.map(customerDto, customerRXInfo);
            return customerRXInfo;
        }
        else if(isRxString){

            RxDto rxDto = objectMapper.readValue(streamData, RxDto.class);
            ModelMapper modelMapper = new ModelMapper();
            RXInfo rXInfo = new RXInfo();
            modelMapper.map(rxDto, rXInfo);
            return rXInfo;
        }
        return null;
    }

    @After
    public void teardown() {
        testDriver.close();
    }
}
