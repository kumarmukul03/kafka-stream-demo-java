package com.wba.api.processrxinfo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@EnableAutoConfiguration
@SpringBootApplication
public class processRXInfoApplication {

    public static void main(String[] args) {
        SpringApplication.run(com.wba.api.processrxinfo.processRXInfoApplication.class, args);
    }


}