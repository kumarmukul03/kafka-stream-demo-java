package com.wba.api.processrxinfo.service;

import com.wba.api.processrxinfo.model.CustomerRXInfo;
import com.wba.api.processrxinfo.repository.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerRXServiceImpl implements CustomerRXService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerRXServiceImpl.class);
    @Autowired
    CustomerRepository customerRepository;

    @Override
    public void insertOrUpdateCustomerRxObj(CustomerRXInfo customerRXInfo, String customerID) {

        try {

            LOGGER.info("Querying DB by using customerID : {}", customerID);
            Optional<CustomerRXInfo> customerRxInfoExsisting = customerRepository.findById(customerID);

            if (customerRxInfoExsisting.isPresent()) {

                LOGGER.info("exiting customer : {} id RXdetails merged with new data", customerID);

                if(customerRxInfoExsisting.get().getRxList().stream().noneMatch(entry->entry.equals(customerRXInfo.getRxList().get(0)))){

                    LOGGER.info("new prescription info identified ... prescription update required ...");
                    customerRXInfo.getRxList().addAll(customerRxInfoExsisting.get().getRxList());
                }
                else{

                    LOGGER.info("Duplicated prescription data found.....hence....prescription update no required...");
                    customerRXInfo.setRxList(customerRxInfoExsisting.get().getRxList());
                }
            }

            if(!customerRxInfoExsisting.isPresent() || !customerRXInfo.equals(customerRxInfoExsisting.get()))
            {
                LOGGER.info("Saving customerRXInfo Object.....");
                customerRepository.save(customerRXInfo);
                LOGGER.info("customerRXInfo successfully saved..");

            }
            else{

                LOGGER.info("duplicated customer info data trying to save.....save operation not required ...");
            }

        } catch (Exception e) {

            LOGGER.error("Exception occurs while doing insert/update/findBy DB operation...please ensure DB service is UP&running", e);
        }
    }
}
