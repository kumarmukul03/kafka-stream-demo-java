package com.wba.api.processrxinfo.service;

import com.wba.api.processrxinfo.model.CustomerRXInfo;

public interface CustomerRXService {

    public void insertOrUpdateCustomerRxObj(CustomerRXInfo customerRXInfo, String customerID);
}
