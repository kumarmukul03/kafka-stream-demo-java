package com.wba.api.processrxinfo.model;

import java.io.Serializable;
import java.util.Objects;

public class RXInfo implements Serializable {

    private String patientId;
    private String rxNumber;
    private String rxDate;
    private String prescriber;
    private String drugId;
    private String storeId;

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getRxNumber() {
        return rxNumber;
    }

    public void setRxNumber(String rxNumber) {
        this.rxNumber = rxNumber;
    }

    public String getRxDate() {
        return rxDate;
    }

    public void setRxDate(String rxDate) {
        this.rxDate = rxDate;
    }

    public String getPrescriber() {
        return prescriber;
    }

    public void setPrescriber(String prescriber) {
        this.prescriber = prescriber;
    }

    public String getDrugId() {
        return drugId;
    }

    public void setDrugId(String drugId) {
        this.drugId = drugId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RXInfo rxInfo = (RXInfo) o;
        return Objects.equals(patientId, rxInfo.patientId) && Objects.equals(rxNumber, rxInfo.rxNumber) && Objects.equals(rxDate, rxInfo.rxDate) && Objects.equals(prescriber, rxInfo.prescriber) && Objects.equals(drugId, rxInfo.drugId) && Objects.equals(storeId, rxInfo.storeId);
    }

}
