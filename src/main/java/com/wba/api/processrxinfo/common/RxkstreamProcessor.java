package com.wba.api.processrxinfo.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wba.api.processrxinfo.dto.CustomerDto;
import com.wba.api.processrxinfo.dto.RxDto;
import com.wba.api.processrxinfo.model.CustomerRXInfo;
import com.wba.api.processrxinfo.model.RXInfo;
import com.wba.api.processrxinfo.service.CustomerRXService;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.kstream.KStream;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.Properties;

@Component
public class RxkstreamProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(RxkstreamProcessor.class);

    @Autowired
    CustomerRXService customerRXService;

    @Value("${kafka.stream.appid}")
    private String applicationId;

    @Value("${kafka.stream.server}")
    private String server;

    @Value("${kafka.topic.customer}")
    private String customerTopic;

    @Value("${kafka.topic.rx}")
    private String rxTopic;

    @PostConstruct
    public void processRXdata() {

        Topology topology = createTopology(customerTopic, rxTopic);
        final KafkaStreams streams = new KafkaStreams(topology, getConfig(applicationId, server));
        streams.start();
    }

    /**
     * Method used for creating Topology
     * @return Topology
     */
    public Topology createTopology(String customerTopic, String rxTopic){

        final StreamsBuilder builder = new StreamsBuilder();

        /*
        Join two topics and construct combined data
         */
        KStream<Object, Object> joinedKStream = builder.stream(customerTopic).join(
                builder.stream(rxTopic), (customerValue, rxValue) -> buildCustomerRxObjFromStreamingData((String) customerValue, (String) rxValue),
                JoinWindows.of(Duration.ofHours(8).toMillis()));

        /**
         * Iterate through newly constructed processed streams data
         */
        joinedKStream.foreach((key, value) -> {

            if(value!=null){

                LOGGER.info("Start processing joined/processed K-streams data .... ");
                CustomerRXInfo customerRxInfo = (CustomerRXInfo) value;
                customerRxInfo.setId((String) key);
                customerRXService.insertOrUpdateCustomerRxObj(customerRxInfo, (String) key); //Save builded info to DB
            }

        });

        return builder.build();
    }

    /**
     * Method for generating TopologyConfig
     */
    public Properties getConfig(String applicationId,String boostrapServerUrl){

        /* ------------------------------ Kstream Configuration ----------------------------------- */
        final Properties properties = new Properties();
        properties.setProperty(StreamsConfig.APPLICATION_ID_CONFIG, applicationId);
        properties.setProperty(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, boostrapServerUrl);
        properties.setProperty(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        properties.setProperty(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        /*------------------------------------------------------------------------------------------*/
        return properties;
    }

    /**
     * @param customerStreamData
     * @param rxStreamData
     * @return CustomerRXInfo
     * <p>
     * Method used for building CustomerRXInfo Model from kafka streams data
     */
    public CustomerRXInfo buildCustomerRxObjFromStreamingData(String customerStreamData, String rxStreamData) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {

            CustomerDto customerDto = objectMapper.readValue(customerStreamData, CustomerDto.class);
            LOGGER.info("customerDto successfully constructed from customerInfo stream ...");

            RxDto rxDto = objectMapper.readValue(rxStreamData, RxDto.class);
            LOGGER.info("RxDto successfully constructed from rxInfo stream ...");

            ModelMapper modelMapper = new ModelMapper();
            CustomerRXInfo customerRXInfo = new CustomerRXInfo();
            modelMapper.map(customerDto, customerRXInfo);

            RXInfo rxInfo = new RXInfo();
            modelMapper.map(rxDto, rxInfo);
            customerRXInfo.getRxList().add(rxInfo);
            LOGGER.info("customerRXInfo com.wba.api.processrxinfo.model successfully constructed from corresponding Dto's ...");

            return customerRXInfo;

        } catch (Exception e) {

            LOGGER.error("Exception occurs while building CustomerRXInfo from kafka stream input...");
            return null;
        }
    }

}