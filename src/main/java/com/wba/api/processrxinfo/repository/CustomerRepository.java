package com.wba.api.processrxinfo.repository;

import com.wba.api.processrxinfo.model.CustomerRXInfo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends MongoRepository<CustomerRXInfo, String> {

}
